<?php


namespace Shop\Model;


use Shop\Service\DataBase;

class UserLogin
{
    private \Medoo\Medoo $dbConnect;

    /**
     * UserLogin constructor.
     */
    public function __construct()
    {
        $this->dbConnect = DataBase::getInstance();
    }

    public function login($login, $password):bool
    {
        $result = $this->dbConnect->select(
            'userlog', ['login'],
            [
                'AND'=>[
                    'login'=>$login,
                    'password'=>$password
                ]
            ]
        );

        if(!empty($result)){
            session_status();
            $_SESSION['userlog'] = $result[0]['login'];
            return true;
        } else{
            return false;
        }
    }

    public function validateUser():bool
    {
        return !empty($_SESSION['userlog']);
    }



}