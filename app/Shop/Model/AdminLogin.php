<?php

namespace Shop\Model;

use Shop\Service\DataBase;

class AdminLogin
{
    private \Medoo\Medoo $dbConnect;

    public function __construct()
    {
        $this->dbConnect = DataBase::getInstance();
    }

    public function login($login, $password): bool
    {
        $result = $this->dbConnect->select(
            'user', ['login'],
            [
                "AND" => [
                    'login' => $login,
                    'password' => $password
                ]
            ]
        );
        if (!empty($result)) {
            session_status();
            $_SESSION['user'] = $result[0]['login'];
            return true;
        } else {
            return false;
        }
    }

    public function validateAdmin(): bool
    {
        return !empty($_SESSION['user']);
    }
}