<?php

namespace Shop\Model;

use Shop\Service\DataObject;

class Product extends DataObject
{
    const FIELDS = ['entity_id', 'name', 'description', 'price'];

    public function getId(): int
    {
        return (int) $this->_data['entity_id'];
    }

    public function getName(): string
    {
        return $this->_data['name'];
    }

    public function setName(string $name): Product
    {
        $this->_data['name'] = $name;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->_data['description'];
    }

    public function setDescription($description): Product
    {
        $this->_data['description'] = $description;
        return $this;
    }

    public function getPrice(): float
    {
        return (float) $this->_data['price'];
    }

    public function setPrice($price): Product
    {
        $this->_data['price'] = $price;
        return $this;
    }
}
