<?php

namespace Shop\Controller;

class AdminLogin extends AbstractController
{
    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        return $this->render('adminhtml/login.html.twig');
    }
}