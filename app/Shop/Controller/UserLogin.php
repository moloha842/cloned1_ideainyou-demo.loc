<?php
namespace Shop\Controller;

class UserLogin extends AbstractController
{


    /**
     * @param \Klein\Request $request
     * @param \Klein\Response $response
     * @return mixed
     */
    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
       return $this->render('userhtml/userlogin.html.twig');
    }
}

