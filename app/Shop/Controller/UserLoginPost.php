<?php

namespace Shop\Controller;

class UserLoginPost extends AbstractController
{
    private \Shop\Model\UserLogin $userLogin;


    public function __construct(
        \Shop\Model\UserLogin $userLogin
    )
    {
        $this->userLogin = $userLogin;
    }

    /**
     * @param \Klein\Request $request
     * @param \Klein\Response $response
     * @return mixed
     */
    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        if ($this->userLogin->login($_POST['login'], $_POST['password'])){
            //login success
            $response->redirect('admin/userdashboard')->send();
        } else {
            //login fail
            $response->redirect('/user')->send();
        }

    }
}
