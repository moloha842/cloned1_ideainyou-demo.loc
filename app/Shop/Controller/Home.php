<?php

namespace Shop\Controller;

use Shop\Model\Product;
use Shop\Service\DataObject;

class Home extends AbstractController
{
    private DataObject $dataObject;
    private Product $product;

    public function __construct(
        DataObject $dataObject,
        Product $product
    ) {
        $this->dataObject = $dataObject;
        $this->product = $product;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        $product = $this->product;
        $product->setName('PrName')
            ->setDescription('Desc')
            ->setPrice('10$');

        return $this->render('home.html.twig', [
            'message' => 'Hello Message'
        ]) . $this->render('productView.html.twig', ['productData' => $product->getData()]);
    }
}