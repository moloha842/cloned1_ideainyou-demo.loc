<?php

namespace Shop\Controller\Admin;

use Shop\Controller\Admin\AbstractController;

class Dashboard extends AbstractController
{
    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        parent::execute($request, $response);
        return $this->render('adminhtml/dashboard.html.twig');
    }
}