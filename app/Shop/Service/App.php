<?php

namespace Shop\Service;

use Medoo\Medoo;

class App
{
    private array $routes;
    public function __construct()
    {
        require_once 'app/config/routes.php';
        $this->routes = $routes;
//        $this->routes = include 'app/config/routes.php';
    }

    public function run()
    {
        $router = new Router($this->routes);
        $router->dispatch();
    }
//    private array $dbConfig;
//
//    /**
//     * App constructor.
//     * @param array $routes
//     * @param array $dbConfig
//     */
//    public function __construct(
//        array $routes,
//        array $dbConfig
//    ) {
//        $this->routes = $routes;
//        $this->dbConfig = $dbConfig;
//    }
//
//    public function run() {
//        $database = new Medoo($this->dbConfig);
//
////        $database->insert("account", [
////            "user_name" => "foo",
////            "email" => "foo@bar.com"
////        ]);
//
//        $router = new Router($this->routes);
//        $router->dispatch();
//    }
}