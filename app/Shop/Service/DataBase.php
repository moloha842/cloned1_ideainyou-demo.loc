<?php

namespace Shop\Service;

use Medoo\Medoo;

class DataBase
{
    static function getInstance(): Medoo
    {
        require_once 'app/config/db.php';
        return new Medoo($dbConfig);
    }
}