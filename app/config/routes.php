<?php

$routes = [
    [
        'method' => "GET",
        'path' => "/",
        'className' => \Shop\Controller\Home::class
    ],
    [
        'method' => "GET",
        'path' => "/admin",
        'className' => \Shop\Controller\AdminLogin::class
    ],
    [
        'method' => "POST",
        'path' => "/admin-login",
        'className' => \Shop\Controller\AdminLoginPost::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/dashboard",
        'className' => \Shop\Controller\Admin\Dashboard::class
    ],
];
