This is a demo project for first year students from <a href="ideainyou.com" target="_blank">IdeaInYou</a> company.

It is intended for mastering the basics of building a web application, CMS and understanding the basic patterns and approaches in programming.

This course is a preparation for further study Magento 2.

Before start:
1. Clone the project
2. ```composer install```
3. Create file ```app/config/db.php```, example:

```
<?php

$dbConfig = [
    'database_type' => 'mysql',
    'database_name' => 'db_name',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'password'
];
```
